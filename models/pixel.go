package models

// Pixel struct example
type RGBA struct {
	R int
	G int
	B int
	A int
}

type RGB struct {
	R int
	G int
	B int
}

type RGBBinary struct {
	R string
	G string
	B string
}
