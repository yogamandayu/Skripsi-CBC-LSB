package libs

import (
	m "cbc-lsb/models"
	"log"
	"math"
	"sync"
)

func GenerateIV(pixels *[][]m.RGBA, width, height int) (x, y int) {

	/*
		===============NOTES================

		ROW = HEIGHT = Coordinate Y = I
		COLUMN = WIDTH = Coordinate X = J

		====================================

	*/

	var rW, cW []m.RGB //row weight,col weight
	var mR, mC m.RGB   //mean row weight, mean col weight
	var posX, posY int //x and y position for IV

	log.Println("Generate Initialization Vector")

	px := *pixels

	var wGMean sync.WaitGroup
	wGMean.Add(2)

	go func() {
		for i := 0; i < height; i++ { //Get row weigth
			var cR m.RGB //count row
			for j := 0; j < width; j++ {
				cR.R += px[i][j].R
				cR.G += px[i][j].G
				cR.B += px[i][j].B
			}
			cR.R = cR.R / width
			cR.G = cR.G / width
			cR.B = cR.B / width
			rW = append(rW, cR)
		}
		for _, v := range rW { //sum all row RGB
			mR.R += v.R
			mR.G += v.G
			mR.B += v.B
		}
		mR.R = mR.R / len(rW) //mean all row RGB
		mR.G = mR.G / len(rW)
		mR.B = mR.B / len(rW)

		var min float64
		for i, v := range rW { // search the minimum difference between row weight and every pixel RGB, and the result is a Y location for IV
			x := math.Abs(float64(v.R-mR.R)) + math.Abs(float64(v.G-mR.G)) + math.Abs(float64(v.B-mR.B))
			if i == 0 {
				min = x
				posY = i
			}
			if x < min {
				min = x
				posY = i
			}
		}
		wGMean.Done()
	}()

	go func() {
		for i := 0; i < width; i++ { //Get column weigth
			var cC m.RGB //count column
			for j := 0; j < height; j++ {
				cC.R += px[j][i].R
				cC.G += px[j][i].G
				cC.B += px[j][i].B
			}
			cC.R = cC.R / width
			cC.G = cC.G / width
			cC.B = cC.B / width
			cW = append(cW, cC)
		}
		for _, v := range cW { //sum all column RGB
			mC.R += v.R
			mC.G += v.G
			mC.B += v.B
		}
		mC.R = mC.R / len(cW) //mean all column RGB
		mC.G = mC.G / len(cW)
		mC.B = mC.B / len(cW)

		var min float64
		for i, v := range cW { // search the minimum difference between column weight and every pixel RGB, and the result is a X location for IV
			x := math.Abs(float64(v.R-mC.R)) + math.Abs(float64(v.G-mC.G)) + math.Abs(float64(v.B-mC.B))
			if i == 0 {
				min = x
				posX = i
			}
			if x < min {
				min = x
				posX = i
			}
		}
		wGMean.Done()
	}()
	wGMean.Wait()

	return posX, posY
}
