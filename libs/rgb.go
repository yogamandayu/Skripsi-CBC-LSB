package libs

import (
	m "cbc-lsb/models"
	"fmt"
	"strconv"
	"strings"
)

func BinRgbToIntRgb(rgb m.RGBBinary) *m.RGB {
	return &m.RGB{
		R: BinToInt(rgb.R, 2),
		G: BinToInt(rgb.G, 2),
		B: BinToInt(rgb.B, 2),
	}
}

func IntRgbToBinRgb(rgb *m.RGB) m.RGBBinary {
	return m.RGBBinary{
		R: Normalize8BitBin(strconv.FormatInt(int64(rgb.R), 2)),
		G: Normalize8BitBin(strconv.FormatInt(int64(rgb.G), 2)),
		B: Normalize8BitBin(strconv.FormatInt(int64(rgb.B), 2)),
	}
}

func StringToBin(s string) string {
	var res string
	for _, c := range s {
		res += fmt.Sprintf("%08b", byte(c))
	}
	return res
}

func BinToString(s string) string {
	var subStr, str string
	i := 0
	for _, r := range s {
		i++
		subStr += string(r)
		if i%8 == 0 {
			str += string(BinToInt(subStr, 2))
			subStr = ""
			i = 0
		}
	}
	return str
}

func SplitBinTo2Bit(s string) []string {
	var str []string
	res := s
	i := 1
	for _, r := range res {
		str = append(str, string(r))
		if i%2 == 0 {
			str = append(str, ",")
		}
		i++
	}
	sx := strings.Join(str, "")
	arrayString := strings.Split(sx, ",")
	return arrayString[0 : len(arrayString)-1]
}

func AllBinToHex(s string) string {
	var subStr, str string
	i := 0
	for _, r := range s {
		i++
		subStr += string(r)
		if i%4 == 0 {
			str += BinToHex(subStr)
			subStr = ""
			i = 0
		}
	}
	return str
}

func AllHexToBin(s string) string {
	var str string
	for _, r := range s {
		str += HexToBin(string(r))
	}
	return str
}

func BinToInt(bin string, length int) int {
	x, _ := strconv.ParseInt(bin, length, 64)
	return int(x)
}

func IntToBin(value int) string {
	return strconv.FormatInt(int64(value), 2)
}

func Normalize8BitBin(bin string) string {
	var str []string
	sum := 8 - len(bin)
	if sum == 0 {
		return bin
	}
	for i := 0; i < sum; i++ {
		str = append(str, "0")
	}
	str = append(str, bin)
	s := strings.Join(str, "")
	return s
}

func Normalize2BitBin(bin string) string {
	var str []string
	sum := 2 - len(bin)
	if sum == 0 {
		return bin
	}
	for i := 0; i < sum; i++ {
		str = append(str, "0")
	}
	str = append(str, bin)
	s := strings.Join(str, "")
	return s
}

func GetBiggestBinInRGB(rgb *m.RGB) (*int, string) {

	n := &rgb.R
	s := "R"
	if rgb.G > *n {
		n = &rgb.G
		s = "G"
	}
	if rgb.B > *n {
		n = &rgb.B
		s = "B"
	}
	return n, s
}

func BinToHex(s string) string {
	ui, err := strconv.ParseUint(s, 2, 64)
	if err != nil {
		return "error"
	}

	return fmt.Sprintf("%x", ui)
}

func HexToBin(hex string) string {
	ui, err := strconv.ParseUint(hex, 16, 64)
	if err != nil {
		return ""
	}

	// %016b indicates base 2, zero padded, with 16 characters
	return fmt.Sprintf("%04b", ui)
}

func GetBinInRGBBin(s string, rgbBin m.RGBBinary) (b string) {
	switch s {
	case "R":
		{
			b = rgbBin.R
		}
	case "G":
		{
			b = rgbBin.G

		}
	case "B":
		{
			b = rgbBin.B
		}
	}
	return
}

func ChangeMSB(rgb *int, newMSB string) {
	var str string
	rgbBin := Normalize8BitBin(IntToBin(*rgb))
	for i, r := range rgbBin {
		if i > 1 {
			str += string(r)
		}
		if i == 0 {
			str += newMSB
		}
	}
	rgbBin = Normalize8BitBin(str)
	*rgb = BinToInt(rgbBin, 2)
}

func XOR(bin1 string, bin2 string) string {
	return Normalize2BitBin(IntToBin(BinToInt(bin1, 2) ^ BinToInt(bin2, 2)))
}
