package libs

import (
	m "cbc-lsb/models"
	"image"
	"io"
)

func ImageToRGBConverter(file io.Reader) (*[][]m.RGBA, int, int, error, image.Image) { //Get Every Pixel and other properties in Image
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, 0, 0, err, nil
	}

	bounds := img.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y

	var pixels [][]m.RGBA
	for y := 0; y < height; y++ {
		var row []m.RGBA
		for x := 0; x < width; x++ {
			row = append(row, RgbaToPixel(img.At(x, y).RGBA()))
		}
		pixels = append(pixels, row)
	}
	return &pixels, width, height, nil, img
}

// img.At(x, y).RGBA() returns four uint32 values; we want a Pixel
func RgbaToPixel(r, g, b, a uint32) m.RGBA {
	rx := int(r / 257)
	gx := int(g / 257)
	bx := int(b / 257)
	ax := int(a / 257)
	return m.RGBA{rx, gx, bx, ax}
}
