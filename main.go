package main

import (
	"bufio"
	"cbc-lsb/helpers"
	"cbc-lsb/libs"
	"cbc-lsb/models"
	"fmt"
	"image"
	"image/jpeg"
	"log"
	"os"
)

func main() {
	/*
		===============NOTES================

		ROW = HEIGHT = Coordinate Y = I
		COLUMN = WIDTH = Coordinate X = J

		===============PROGRESS==============

		Kerjakan yang bagian geser KEY

		=====================================
	*/

	var err error

	// You can register another format here
	image.RegisterFormat("jpeg", "jepg", jpeg.Decode, jpeg.DecodeConfig)
	fileImg := "key.jpg"

	file, err := os.Open(`./` + fileImg)
	if err != nil {
		log.Fatal("Key image not found, place your key.jpg in same location with this file.")
	} else {
		fmt.Println("Key image found")
	}
	defer file.Close()
	pixels, width, height, err, _ := libs.ImageToRGBConverter(file)
	if err != nil {
		log.Fatal(err.Error())
	}
	x, y := libs.GenerateIV(pixels, width, height)

	iv := libs.IntRgbToBinRgb(&models.RGB{
		R: (*pixels)[y][x].R,
		G: (*pixels)[y][x].G,
		B: (*pixels)[y][x].B,
	})

	var option string
	var key models.RGB
	var cipher, plain string
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Encrypt or Decrypt ? E/D")
	fmt.Scan(&option)
	fmt.Println("Input your key")
	fmt.Println("Red?")
	fmt.Scan(&key.R)
	fmt.Println("Green?")
	fmt.Scan(&key.G)
	fmt.Println("Blue?")
	fmt.Scan(&key.B)
	switch option {
	case "E":
		{
			fmt.Println("Input your plaintext")
			if scanner.Scan() {
				plain = scanner.Text()
			}
			binPt := libs.StringToBin(plain)
			spt := libs.SplitBinTo2Bit(binPt)                                    //Array Binary Plain Text, splitted by 2 binary
			cipher = helpers.Encrypt(spt, key, iv, *pixels, width, height, x, y) //Encrypt Plain Text
			fmt.Println("This is your ciphertext, DON'T TELL ANYONE!!!")
			fmt.Println(cipher)
		}
	case "D":
		{
			fmt.Println("Input your ciphertext")
			if scanner.Scan() {
				cipher = scanner.Text()
			}
			binPt := libs.AllHexToBin(cipher)                                   //Turn ciphertext into binary
			sbc := libs.SplitBinTo2Bit(binPt)                                   //split ciphertext binary by 2 binary
			plain = helpers.Decrypt(sbc, key, iv, *pixels, width, height, x, y) //Decrypt Cipher Text
			fmt.Println("This is your plaintext, DON'T TELL ANYONE!!!")
			fmt.Println(plain)
		}
	default:
		{
			log.Fatal("Invalid Input")
		}
	}
}
