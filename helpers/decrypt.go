package helpers

import (
	"cbc-lsb/libs"
	m "cbc-lsb/models"
)

func Decrypt(s []string, key m.RGB, iv m.RGBBinary, px [][]m.RGBA, width, height, x, y int) string {
	var plain string
	for _, v := range s {
		n, s := libs.GetBiggestBinInRGB(&key)
		biv := libs.Normalize8BitBin(libs.GetBinInRGBBin(s, iv))
		rx := XOREncryptDecrypt(libs.Normalize8BitBin(libs.IntToBin(*n)), biv, v)
		libs.ChangeMSB(n, v)
		plain += rx
		a := GetNextPixels(px, &key, s, width, height, &x, &y)
		iv = libs.IntRgbToBinRgb(a)
	}
	return libs.BinToString(plain)
}
