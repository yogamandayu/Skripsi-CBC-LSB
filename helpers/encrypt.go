package helpers

import (
	"cbc-lsb/libs"
	m "cbc-lsb/models"
	"math"
)

func Encrypt(s []string, key m.RGB, iv m.RGBBinary, px [][]m.RGBA, width, height, x, y int) string { //Encrypt Process
	var cipher string
	for _, v := range s {
		n, s := libs.GetBiggestBinInRGB(&key)                                     //Get the biggest RGB value in key
		biv := libs.Normalize8BitBin(libs.GetBinInRGBBin(s, iv))                  //Get binary in IV depends on biggest RGB in key, if the biggest is in Red, so it will get Red Value in IV
		rx := XOREncryptDecrypt(libs.Normalize8BitBin(libs.IntToBin(*n)), biv, v) //XOR LSB of Biggest Value in Key, IV, and the binary Plain Text, the result will be the ciphertext
		libs.ChangeMSB(n, rx)                                                     //Change the Biggest RGB Value in Key MSB, why MSB because if we change the LSB, the next RGB value will never change
		cipher += rx                                                              //Append XOR result
		a := GetNextPixels(px, &key, s, width, height, &x, &y)                    //Get the next pixels to encrypt the next binary plain text
		iv = libs.IntRgbToBinRgb(a)                                               //IV will be change into next pixel RGB Value
	}
	return libs.AllBinToHex(cipher)
}

func XOREncryptDecrypt(bK, bIV, s string) string {
	return libs.XOR(bK[6:8], libs.XOR(bIV[6:8], s))
}

func GetNextPixels(px [][]m.RGBA, key *m.RGB, s string, width, height int, x, y *int) *m.RGB {
	var c string
	binKey := libs.IntRgbToBinRgb(key)
	switch s {
	case "R":
		{
			c = binKey.G + binKey.B
		}
	case "G":
		{
			c = binKey.R + binKey.B
		}
	case "B":
		{
			c = binKey.R + binKey.G
		}
	}
	sv := libs.BinToInt(c, 2)
	sv += *x                                            //Sum with current column position)
	*y += int(math.Floor(float64(sv) / float64(width))) //Get new Row position
	*x = sv % width
	if *y >= height {
		*y = *y % height //if Y is more than height, start from Row 0
	}
	newPX := (px)[int(*y)][*x]

	return &m.RGB{
		R: newPX.R,
		G: newPX.G,
		B: newPX.B,
	}
}
